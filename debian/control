Source: libtime-moment-perl
Maintainer: Debian Perl Group <pkg-perl-maintainers@lists.alioth.debian.org>
Uploaders: Nick Morrott <knowledgejunkie@gmail.com>
Section: perl
Testsuite: autopkgtest-pkg-perl
Priority: optional
Build-Depends: debhelper-compat (= 13),
               libdatetime-perl <!nocheck>,
               libextutils-parsexs-perl,
               libjson-xs-perl <!nocheck>,
               libparams-coerce-perl <!nocheck>,
               libsereal-perl <!nocheck>,
               libtest-fatal-perl <!nocheck>,
               libtest-number-delta-perl <!nocheck>,
               libtest-requires-perl <!nocheck>,
               perl-xs-dev,
               perl:native
Standards-Version: 4.6.1
Vcs-Browser: https://salsa.debian.org/perl-team/modules/packages/libtime-moment-perl
Vcs-Git: https://salsa.debian.org/perl-team/modules/packages/libtime-moment-perl.git
Homepage: https://metacpan.org/release/Time-Moment
Rules-Requires-Root: no

Package: libtime-moment-perl
Architecture: any
Depends: ${misc:Depends},
         ${perl:Depends},
         ${shlibs:Depends}
Description: Perl C/XS module representing date and time of day with UTC offset
 Time::Moment is an immutable object representing a date and time of day with
 an offset from UTC in the ISO 8601 calendar system.
 .
 Time is measured in nanoseconds since 0001-01-01T00Z. Leap seconds are
 ignored.  Time::Moment can represent all epoch integers from -62,135,596,800
 to 2,534,02,300,799; this range suffices to measure times to nanosecond
 precision for any instant that is within 0001-01-01T00:00:00Z to
 9999-12-31T23:59:59Z.
